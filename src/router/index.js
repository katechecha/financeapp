import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/stats',
      name: 'stats',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/Stats.vue')
    },
    {
      path: '/stats-table',
      name: 'statsTable',
      component: () => import('../views/StatsTable.vue')
    },
    {
      path: '/add',
      name: 'add',
      component: () => import('../views/AddCost.vue')
    },
    { 
      path: '/:pathMatch(.*)', 
      component: () => import('../views/404.vue') 
    }
    
  ]
})

export default router
