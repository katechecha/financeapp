import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import { library } from '@fortawesome/fontawesome-svg-core'
import { dom } from "@fortawesome/fontawesome-svg-core";


/* import font awesome icon component */

/* import specific icons */
import { faXmark } from '@fortawesome/free-solid-svg-icons'

import { createStore } from 'vuex'

import Datepicker from '@vuepic/vue-datepicker';
import '@vuepic/vue-datepicker/dist/main.css'

import LoDash from 'lodash'
import moment from 'moment';

import Vuelidate from '@vuelidate/core'

import axios from 'axios'
import VueAxios from 'vue-axios'


library.add(faXmark)

dom.watch();

const app = createApp(App)
app.component('Datepicker', Datepicker);
app.use(LoDash)
app.use(router)
app.use(createStore)
app.use(moment)
app.use(Vuelidate)
app.use(VueAxios, axios)


app.mount('#app')
